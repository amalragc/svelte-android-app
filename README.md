# Steps

## Install android studio

```
https://developer.android.com/studio/install
```

## Install framework7 cli
```
npm install -g framework7-cli
```

## Create app
To create Framework7 app, run the following command in the directory where you want to create the app:
```
framework7 create --ui
```

## Configuring the app
* select capacitor
* framework 7 with svelte
* create

## Go to the svelte app directory
```
 npm i
 npm install @capacitor/android
 npx cap add android
```

Make all the changes need to be done in the frontend application

## Steps to create Android application
Run the below command after you have made all the changes to be done in the frontend application.
```
npm run build
npx cap sync
```
After the above steps,
open android folder present in the svelte app directory in Android studio

## Inside android studio
click these in the menu bar
```
build -> make project
build bundles(s)/ APK(s) -> Build APK(s)
```

After these steps, apk will be created in the below folder
```
android/app/build/outputs/apk/debug/app-debug.apk
```

## Capacitor Documentation

```
https://capacitorjs.com/docs/basics/workflow

https://capacitorjs.com/docs/android
```

## Adding enviornment variables
create a .env file in your project root folder with a VITE_* variable:
```
VITE_url = "amal.com/1.png"
```
Then you can access this variable in a .js or .ts module:
```
export const URL = import.meta.env.VITE_url;
console.log("URL from env: ",URL)
```

Official svelete documentation link
```
https://kit.svelte.dev/faq
```

